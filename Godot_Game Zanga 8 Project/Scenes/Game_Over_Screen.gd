extends CanvasLayer


export (String,FILE,"*.tscn") var first_scene

func _on_Play_Again_button_up():
	get_tree().change_scene(first_scene)
	pass 


func _on_Quit_button_up():
	get_tree().quit()
	pass 

func _on_itch_io_button_up():
	OS.shell_open("https://grasswhooper.itch.io/")
	pass 

func _on_Facebook_button_up():
	OS.shell_open("https://www.facebook.com/motasem.zakarneh.94")
	pass 

func _on_Twitter_button_up():
	OS.shell_open("https://twitter.com/MoMutrz")
	pass 