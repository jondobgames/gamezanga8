extends KinematicBody2D

export (NodePath) var rest_position

var player_movement
var player_interacter
var player_anim_cont

func _ready():
	player_movement = get_node("Player Movement")
	player_interacter = get_node("Player Interacter")
	player_anim_cont = $PlayerAnimationController
	
	player_interacter.connect("started_interaction",self,"_on_started_interaction")
	player_interacter.connect("finished_interaction",self,"_on_finished_interaction")
	pass

func re_set_position():
	if(has_node(rest_position)):
		var rest_pos_node = get_node(rest_position)
		player_movement.warp(rest_pos_node.global_position)
	pass

func _disable_movement():
	player_movement.disable_movement()
	pass

func _enable_movement():
	player_movement.enable_movement()
	pass

func _process(delta):
	player_anim_cont.update_movement(player_movement.movementDir,player_movement.last_non_zero_dir)
	pass

func _on_started_interaction():
	_disable_movement()
	player_anim_cont.interact(player_movement.last_non_zero_dir)
	pass

func _on_finished_interaction():
	_enable_movement()
	pass