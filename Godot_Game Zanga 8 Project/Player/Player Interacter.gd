extends Area2D

signal started_interaction
signal finished_interaction

var is_interacting=false
var interactables = []
var current_interaction_index = 0

func _on_Player_Interaction_area_entered(area):
	var entered_interactable = area
	
	if(not interactables.has(entered_interactable) and entered_interactable.activity):
		interactables.append(entered_interactable)
		_connect_interactable(entered_interactable)
		Global.GUI.show_tip("Interact")
	pass 

func _on_Player_Interaction_area_exited(area):
	var left_interactable = area
	var left_interactable_index = interactables.find(left_interactable)
	
	if(left_interactable_index>=0):
		interactables.remove(left_interactable_index)
		_disconnect_interactable(left_interactable)
		Global.GUI.hide_tip()
	pass

func _connect_interactable(interactable):
	if(interactable!=null):
		if(not interactable.is_connected("completed_interaction",self,"_done_interacting")):
			interactable.connect("completed_interaction",self,"_done_interacting")
	pass

func _disconnect_interactable(interactable):
	if(interactable.is_connected("completed_interaction",self,"_done_interacting")):
			interactable.disconnect("completed_interaction",self,"_done_interacting")
	pass

func _get_interactable_from(area):
	var interactable = null
	if(area.is_in_group(Global.Interactables_Group)):
		interactable = area
	elif(area.is_in_group(Global.Interactables_Triggers_Group)):
		interactable = area.get_interactable()
	return interactable

func _process(delta):
	if(not is_interacting and _is_near_interactable() and Input.is_action_just_pressed("ui_interact")):
		Global.GUI.hide_tip()
		emit_signal("started_interaction")
		is_interacting=true
		interactables[current_interaction_index].interact()
	pass

func _done_interacting():
	current_interaction_index=current_interaction_index + 1
	if (current_interaction_index>=interactables.size()):
		current_interaction_index=0
		is_interacting=false
		emit_signal("finished_interaction")
	else:
		interactables[current_interaction_index].interact()
	pass

func _is_near_interactable():
	return interactables.size()>0