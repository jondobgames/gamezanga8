extends Node

export var movementSpeed = 200
var movementDir = Vector2(0,0)
var last_non_zero_dir = Vector2(0,0)
var player
var enabled = true

func _ready():
	player = get_parent()
	pass
func _process(delta):
	_set_movement_direction()
	pass

func _physics_process(delta):
	_apply_movement()
	pass
func _set_movement_direction():
	if (not enabled):
		return
	
	var up = Input.is_action_pressed("ui_up")
	var down = Input.is_action_pressed("ui_down")
	
	var left = Input.is_action_pressed("ui_left")
	var right = Input.is_action_pressed("ui_right")
	movementDir.x=int(right)-int(left)
	movementDir.y=int(down)-int(up)
	if(movementDir != Vector2(0,0)):
		movementDir = movementDir.normalized()
		last_non_zero_dir=movementDir
	pass

func _apply_movement():
	player.move_and_slide(movementDir*movementSpeed)
	pass

func disable_movement():
	movementDir=Vector2(0,0)
	enabled=false
	pass

func enable_movement():
	enabled=true
	pass

func warp(to_position):
	player.global_position=to_position
	pass