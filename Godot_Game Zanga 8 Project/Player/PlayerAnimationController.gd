extends AnimationPlayer


func update_movement(movementDir,last_non_zero):
	if(movementDir == Vector2(0,0)):
		if(last_non_zero.x>0 or last_non_zero.x==0):
			_try_play_anim("right_idle")
		elif(last_non_zero.x<0):
			_try_play_anim("left_idle")
	elif(movementDir.x>0 or movementDir.x == 0):
		_try_play_anim("right")
	elif(movementDir.x<0):
		_try_play_anim("left")
	pass

func interact(last_non_zero):
	if(last_non_zero.x>0 or last_non_zero.x==0):
		_try_play_anim("right_action")
	else:
		_try_play_anim("left_action")
	pass

func _try_play_anim(anim_name):
	if(current_animation.get_basename()!= anim_name):
		play(anim_name)
	pass