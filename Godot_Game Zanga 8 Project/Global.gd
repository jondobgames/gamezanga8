extends Node

var GUI
var debug=false
const Interactables_Group="Interactable"
const Interactables_Triggers_Group="Interactable Triggers"
const pointers_group = "Pointers"

const stories_file_name = "stories.json"

var stories_data={
"00_Intro":["argh....my head hurts.....so now.... i am the only one left.....",
			"last night, as much as i struggled, but they took my last friend awaynow my turn is about to come i guess......"
			,"enough sleeping on my end, no more sleep......as if i could sleep....."],
"01_Beds":["how i always wished, for we had more of beds, more food","they always had much more than us, i wish they shared with us"
			,"but there was nothing we could do, but watch until we could not move no more"],
"02_Little Birds":["i was the only one to notice these little bird scribbles, there were so many at first "
					,"but now sadly, even as my friends start vanishing one by one, the birds started vanishing along with them, now there are only two"
					,"though i can't seem to remember where the last one came from, lastnight there was only one"],
"03_Lectures":["every time they'd come, we get grouped on the stage, as they'd lecture us"
				,"time we after, we realized after each time we get grouped on the stage, one of us goes away",
				"if only we were good kids like they asked, we tried to listen as best as we could but we just were not good kids"],
"04_Beds_2":["how sad, there were only a handleful of beds but too many of kids so we had to fight or sleep togather"
			,"but now, there is only me, and so many beds, it feels.....painful, it makes it hard to breath"],
"05_Chairs":["am i just going to have to wait....until they come and set on those chairs and then follow my friends?....",
			 "no, i can't stand that, it is just too hurtful, but there is no one to stand by me anymore, its just me..."
			 ,"but many have tried to escape and non made it...."],
"06_Boxes":["those boxes, just kept showing up one by one, as the birds and us have vanished one by one...."
			,"just a box....a box that does nothing.....","a box..... is that what i'll become?",
			"no.... i am not going to become a box, i don't want, i must stop being a coward, i am going to leave this place"],
"07_Door Step": ["why is this door always locked.....why can't it just be open for once, the thing that is splitting me from escpaing"
				,"i'll try and look at the things my friends hid, after being caught,since they'd get sent here for a while but covered in painful red"
				,"then it is not long until they turn into a box"],
"Puzzle_Solved":["it.....it is open.....i.... i got it..... i can be  free.....i can finally leave....."],

"P1_Physics" : ["after being caught, he just came and said those words on the stage while laughing hysterically, and told us to write them down, saying it is a part of the answer"],
"P2_Word":["i wonder, if this scribble has a meaning to it, but i certainly remember how frusterated my friend was while writing this with pain, after being caught"],
"PE1_Bed":["i probably shouldn't sleep right now....they left me this meal....but how can i eat it, now that i am all alone"],
"PE2_Chairs":["there are four chairs, but i don't think knowing that is helpful, i should look at the clues my friends left me"],
"PE3_Boxes":["i am not going to become a box, i'll figure out, the magic word of the door"],
"PE4_Star":["i wonder, whats my star is doing above the boxes, i am sure i didn't leave it there, no time to play with it now, i must find the magic word"],

"2_00_Free":["am i..... free? my god.... i am free.....","i don't care where i go, i am out, there isn't anyone around"],
"2_01_My_Star":["oh, this looks exactly like my star, how surprising, lots of beautiful things all over the place","i wish had gotten out earlier"
				,"i can't believe it.... i don't have to become a box..... i am so happy"],
"2_02_House":["oh, there is a house over there, i should check it out, it surely does look fancy"],
"2_03_At_House":["hello anyone there?","anyone?.....hello?","looks like, i am on my own....","owh, what is this on the ground"],

"3_00_Intro":["i just need to reach the end....avoid the birds.... i can't fail, i must not fail."],
"3_01_Cant_Go_Back":["what closed it....?" ,"regardless of that i can't go back, i don't want to , i must and will keep going forward and only forward"],
"3_02_Made_It":["I Made It......i passed all of the challenges..... i should be free...... i am almost free....i can't believe this......"],
"3_03_Finally_Free":["it is opened...the gate to my freedom"],

"4_00_Why":["no....no.... this can not be happening....","why am i here....i should have.....been out.....what was i even doing"]
}
func _ready():
	#stories_data = get_from_json(stories_file_name)
	pass

func get_story(key):
	return stories_data[key]

func get_from_json(file_name):
	var file = File.new()
	file.open(file_name,File.READ)
	var text = file.get_as_text()
	var data = parse_json(text)
	file.close()
	return data

func shuffle_array(array):
	randomize()
	var shuffled_array=[]
	var array_to_shuffle = array
	var total_size = array.size()
	
	for i in range(total_size):
		var current_size = array_to_shuffle.size()
		var rand_index = randi() % current_size
		var element = array_to_shuffle[rand_index]
		shuffled_array.append(element)
		array_to_shuffle.remove(rand_index)
	
	for e in shuffled_array:
		print(e)
	
	return shuffled_array

func shuffle(array):
	randomize()
	var size = array.size()
	for i in range(size - 1):
		swap(array, i, (randi()%size )+ i)
	return array

func swap(array, i, j):
	var tmp = array[i]
	array[i] = array[j]
	array[j] = tmp
	return array