extends Container

signal finished_fading_in
signal finished_fading_out
signal finished_fading

var animation_player

func _ready():
	animation_player=get_node("AnimationPlayer")
	pass

func fade(to_state):
	if(to_state):
		fade_in()
	else:
		fade_out()
	pass

func fade_in():
	animation_player.play("Fade In")
	pass

func fade_out():
	animation_player.play("Fade Out")
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	emit_signal("finished_fading")
	if(anim_name=="Fade In"):
		emit_signal("finished_fading_in")
	elif(anim_name=="Fade Out"):
		emit_signal("finished_fading_out")
	pass 