extends Container

signal tip_shown
signal tip_hidden
signal tip_switched

export var tips ={ "Interact":"Press [E] To Interact","Attack":"Press [R] To Attack" }

var animation_player
var text
var is_up = false

func _ready():
	animation_player = get_node("AnimationPlayer")
	text = get_node("Body Displacer/Border/Text")
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	emit_signal("tip_switched")
	animation_player.play("Idle")
	if(anim_name=="Show"):
		emit_signal("tip_shown")
		
	elif(anim_name=="Hide"):
		emit_signal("tip_hidden")
	pass 

func show_tip(tip):
	text.text=tips[tip]
	var anim_name = animation_player.current_animation.get_basename()
	if(anim_name!="Show" and not is_up):
		animation_player.play("Show")
		is_up=true
	pass

func hide_tip():
	var anim_name = animation_player.current_animation.get_basename()
	if(anim_name!="Hide" and is_up):
		animation_player.play("Hide")
		is_up=false
	pass