extends CanvasLayer

var tool_tip
var story_teller 
var selection_puzzle
var images_displayer

var level_fade_panel
var pause_name_table = { true : "Resume" , false : "Pause" }
var resume_pause_button

func _ready():
	Global.GUI=self
	images_displayer=$"Images Displayer"
	resume_pause_button=get_node("Buttons Container/Resume_Pause")
	level_fade_panel=get_node("Level Fade Panel")
	level_fade_panel.connect("finished_fading",self,"_enable_buttons")
	
	tool_tip = get_node("Tool Tip")
	
	story_teller = $"Story Teller"
	selection_puzzle=$"Selection Puzzle"
	
	resume_pause_button.disabled = true
	pass

func _on_Resume_Pause_button_up():
	resume_pause_button.disabled = true
	_switch_pause_state()
	_update_pause_visuals()
	pass

func _switch_pause_state():
	get_tree().paused = not get_tree().paused
	pass

func _update_pause_visuals():
	resume_pause_button.text=pause_name_table[get_tree().paused]
	level_fade_panel.fade(get_tree().paused)
	pass
	
func _enable_buttons():
	resume_pause_button.disabled = false
	pass

func _on_Quit_button_up():
	get_tree().quit()
	pass

func show_tip(tip):
	tool_tip.show_tip(tip)
	pass

func hide_tip():
	tool_tip.hide_tip()
	pass

func tell_story(story_to_tell):
	story_teller.tell_story(story_to_tell)
	pass

func display_image(image):
	images_displayer.display_image(image)
	pass