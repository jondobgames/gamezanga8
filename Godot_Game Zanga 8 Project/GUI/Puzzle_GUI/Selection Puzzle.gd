extends Container

export (PackedScene) var mark_button_scene

signal on_good_answer
signal on_bad_answer
signal on_left

var correct_answer
var result_line
var animation_player
var buttons_container
var mark_buttons = []

const show_anim="Show"
const hide_anim="Hide"


func init_puzzle(possiblities,answer):
	correct_answer=answer
	result_line.max_length=correct_answer.length()
	for possiblity in possiblities:
		var mark_button = mark_button_scene.instance()
		buttons_container.add_child(mark_button)
		
		mark_button.initialize(possiblity,self)
		mark_buttons.append(mark_button)
	show()
	pass

func _ready():
	result_line = $"Puzzle Container/Result Line"
	animation_player = $AnimationPlayer
	buttons_container = $"Puzzle Container/Buttons Container"
	hide()
	pass

func show():
	_try_play_anim(show_anim)
	pass

func hide():
	_try_play_anim(hide_anim)
	emit_signal("on_left")
	pass

func _try_play_anim(anim_name):
	if(anim_name!= animation_player.current_animation.get_basename()):
		animation_player.play(anim_name)
	pass

func clicked_on(possiblity):
	if(result_line.text.length()<result_line.max_length):
		result_line.text = result_line.text+possiblity
	pass

func _delete():
	if(result_line.text.length()>0):
		result_line.text[result_line.text.length()-1]=""
	pass

func _on_Deletion_Button_button_up():
	_delete()
	pass

func _on_Answer_Submission_Button_button_up():
	if(correct_answer==result_line.text):
		emit_signal("on_good_answer")
		hide()
	else:
		emit_signal("on_bad_answer")
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name==hide_anim):
		for button in mark_buttons:
			button.queue_free()
		mark_buttons = []
	pass 


func _on_Back_Button_button_up():
	hide()
	pass 
