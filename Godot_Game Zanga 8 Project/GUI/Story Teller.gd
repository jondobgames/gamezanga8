extends Container

signal completed_story
signal hid_teller

export var time_between_letters = 0.1
var letters_timer
var text
var animation_player
var continue_button
var stories_to_tell = []
var current_story_index

func _ready():
	letters_timer= $"Letters Timer"
	letters_timer.wait_time=time_between_letters
	text = $Text
	animation_player=$AnimationPlayer
	continue_button=$"Continue Button"
	pass

func tell_story(stories):
	stories_to_tell=stories
	text.bbcode_text=""
	animation_player.play("Show")
	continue_button.disabled=true
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Show"):
		for i in stories_to_tell.size():
			var story_to_tell = stories_to_tell[i]
			continue_button.disabled=true
			current_story_index = i
			
			for c in story_to_tell:
				text.bbcode_text=text.bbcode_text+c
				letters_timer.start()
				yield(letters_timer,"timeout")
			
			continue_button.disabled=false
			yield (continue_button,"button_up")
		
		emit_signal("completed_story")
	
	pass

func _on_Continue_Button_button_up():
	if(current_story_index>=stories_to_tell.size()-1):
		animation_player.play("Hide")
		emit_signal("hid_teller")
		stories_to_tell=[]
	else:
		text.bbcode_text=""
	pass 