extends Container

signal image_shown
signal image_hidden

export var end_anim = "Hide"
export var start_anim= "Show"
export var idle_anim = "Idle"

var back_button
var image
var animation_player

func _ready():
	back_button = $"Back Button"
	image=$Image
	animation_player = $AnimationPlayer
	pass

func display_image(texture):
	image.texture = texture
	_show()
	pass

func _show():
	back_button.disabled=true
	_try_play_anim(start_anim)
	pass

func _on_Back_Button_button_up():
	back_button.disabled=true
	_try_play_anim(end_anim)
	pass 

func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name==start_anim):
		back_button.disabled=false
		emit_signal("image_shown")
		_try_play_anim(idle_anim)
	elif(anim_name==end_anim):
		emit_signal("image_hidden")
	pass 

func _try_play_anim(anim_name):
	if(animation_player.current_animation.get_basename()!=anim_name):
		animation_player.play(anim_name)
	pass