extends "res://Interactables/Abs_Interactable/Abs_Interactable.gd"

var selection_puzzle_interacter
export var reactions_on_good_answer = false
func _ready():
	if(reactions_on_good_answer):
		permitted_to_call_reactions=false
	selection_puzzle_interacter=$"Selection Puzzle Interacter"
	selection_puzzle_interacter.connect("left_puzzle",self,"_on_left_selection_puzzle")
	selection_puzzle_interacter.connect("good_answer",self,"_on_good_answer")

func interact():
	if(not selection_puzzle_interacter.solved):
		selection_puzzle_interacter.open_puzzle()
	elif(reactions_on_good_answer):
		permitted_to_call_reactions=true
	pass
	

func _on_good_answer():
	if(reactions_on_good_answer):
		permitted_to_call_reactions=true
	pass

func _on_left_selection_puzzle():
	_completed_interaction()
	pass