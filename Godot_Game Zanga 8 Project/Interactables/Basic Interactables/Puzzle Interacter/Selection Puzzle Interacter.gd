extends Node

signal good_answer
signal bad_answer
signal left_puzzle

var solved = false
var generator
var possiblities
var correct_answer
var selection_puzzle

func _ready():
	if(not has_node("Generator")):
		print("There Is No Generator On : "+str(get_path()))
	generator = $Generator
	print(get_parent().get_name())
	generator.generate_lock_data()
	possiblities = generator.get_possiblities()
	correct_answer = generator.get_correct_answer()
	pass

func open_puzzle():
	if(not solved):
		selection_puzzle=Global.GUI.selection_puzzle
		selection_puzzle.init_puzzle(possiblities,correct_answer)
		selection_puzzle.connect("on_left",self,"_on_left_selection_puzzle")
		selection_puzzle.connect("on_good_answer",self,"_on_good_answer")
	pass

func _on_left_selection_puzzle():
	emit_signal("left_puzzle")
	selection_puzzle.disconnect("on_left",self,"_on_left_selection_puzzle")
	selection_puzzle.disconnect("on_good_answer",self,"_on_good_answer")
	pass

func _on_good_answer():
	solved = true
	emit_signal("good_answer")
	pass