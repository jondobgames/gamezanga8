extends Node

export var possiblities = []
export var correct_answer = ""

var instance_possiblities = []

func get_possiblities():
	return instance_possiblities

func get_correct_answer():
	return correct_answer

func generate_lock_data():
	instance_possiblities = _get_instance_possibilities()
	pass

func _get_generated_possibilities():
	return possiblities
	pass