extends "res://Interactables/Basic Interactables/Generators/Simple_Lock_Generator.gd"

func _get_instance_possibilities():
	var answer_possiblities = []
	
	for p in possiblities:
		if (not answer_possiblities.has(p)):
			answer_possiblities.append(p)
	
	for c in correct_answer:
		answer_possiblities.append(c)
	
	answer_possiblities = Global.shuffle_array(answer_possiblities)
	return answer_possiblities