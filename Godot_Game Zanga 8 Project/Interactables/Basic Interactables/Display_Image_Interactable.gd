extends "res://Interactables/Abs_Interactable/Abs_Interactable.gd"


export (Texture) var image
var images_displayer
func interact():
	images_displayer = Global.GUI.images_displayer
	images_displayer.connect("image_hidden",self,"_on_image_hidden")
	images_displayer.display_image(image)
	pass

func _on_image_hidden():
	images_displayer.disconnect("image_hidden",self,"_on_image_hidden")
	_completed_interaction()
	pass