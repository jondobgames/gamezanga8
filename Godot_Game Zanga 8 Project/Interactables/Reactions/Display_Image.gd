extends "res://Interactables/Reactions/Abs_Reaction.gd"

export (Texture) var image

func _react():
	Global.GUI.display_image(image)
	pass