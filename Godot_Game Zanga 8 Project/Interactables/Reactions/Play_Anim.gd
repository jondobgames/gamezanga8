extends "res://Interactables/Reactions/Abs_Reaction.gd"
export (NodePath) var animator_path
export var anim_name = "anim_name"

func _react():
	if(has_node(animator_path)):
		var animator = get_node(animator_path)
		animator.play(anim_name)
	pass