extends Node
export var time_before_activation = 0
export var reaction_duration = 0
signal completed_reaction

func react():
	yield(get_tree().create_timer(time_before_activation), "timeout")
	_react()
	yield(get_tree().create_timer(reaction_duration), "timeout")
	emit_signal("completed_reaction")
	pass

func _react():
	print("Default Reaction")
	pass