extends "res://Interactables/Reactions/Abs_Reaction.gd"

export var set_activity = true
export (NodePath) var interactable_path

func _react():
	if(has_node(interactable_path)):
		var interactable = get_node(interactable_path)
		interactable.set_activity(set_activity)
	pass