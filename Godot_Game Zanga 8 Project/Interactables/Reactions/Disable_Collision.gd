extends "res://Interactables/Reactions/Abs_Reaction.gd"
export (NodePath) var collision_path

func _react():
	if(has_node(collision_path)):
		var collision_node = get_node(collision_path)
		collision_node.set_collision_mask(0)
		collision_node.set_collision_layer(0)
	pass