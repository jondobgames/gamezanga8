extends "res://Interactables/Reactions/Abs_Reaction.gd"

export (NodePath) var interactable_path

func _react():
	print("trying to interact")
	if(has_node(interactable_path)):
		var interactable_node = get_node(interactable_path)
		if(interactable_node.is_in_group(Global.Interactables_Group)):
			interactable_node.interact()
			print("in the correct if")
		else:
			print("Trying To Call Interact With Non Interactable, the path doesn't lead to an interactable")
	pass