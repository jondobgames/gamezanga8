extends "res://Interactables/Reactions/Abs_Reaction.gd"

export (String,FILE,"*.tscn") var scene_to_go_to

func _react():
	get_tree().change_scene(scene_to_go_to)
	pass