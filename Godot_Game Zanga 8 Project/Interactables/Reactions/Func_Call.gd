extends "res://Interactables/Reactions/Abs_Reaction.gd"

export (NodePath) var path
export var func_name = ""

func _react():
	if(has_node(path)):
		var node = get_node(path)
		if(node.has_method(func_name)):
			var the_func = funcref(node,func_name)
			the_func.call_func()
	pass