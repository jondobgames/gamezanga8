extends Node

signal all_reactions_completed
var reactions_count = 0
var completed_reactions_count = 0

func react():
	reactions_count = get_child_count()
	var reactions = get_children()
	for reaction in reactions:
		if(not reaction.is_connected("completed_reaction",self,"_sign_reaction_completion")):
			reaction.connect("completed_reaction",self,"_sign_reaction_completion")
			reaction.react()
	pass

func _sign_reaction_completion():
	completed_reactions_count = completed_reactions_count + 1 
	if(completed_reactions_count>=reactions_count):
		completed_reactions_count = 0
		emit_signal("all_reactions_completed")
	pass