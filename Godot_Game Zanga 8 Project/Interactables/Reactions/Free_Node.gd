extends "res://Interactables/Reactions/Abs_Reaction.gd"
export (NodePath) var path

func _react():
	if(has_node(path)):
		var node = get_node(path)
		node.queue_free()
	pass