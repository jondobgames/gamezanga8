extends "res://Interactables/Reactions/Abs_Reaction.gd"

export var set_activity = true
export (Array,NodePath) var interactables_paths = []

func _react():
	for interactable_path in interactables_paths:
		if(has_node(interactable_path)):
			var interactable = get_node(interactable_path)
			interactable.set_activity(set_activity)
	pass