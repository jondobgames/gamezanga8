extends "res://Interactables/Reactions/Abs_Reaction.gd"
export (NodePath) var path_of_node_to_move
export (NodePath) var path_of_new_position
export var set_pos = true
export var set_rot = true

func _react():
	if(has_node(path_of_node_to_move) and has_node(path_of_new_position)):
		var node_to_move = get_node(path_of_node_to_move)
		var new_position_node = get_node(path_of_new_position)
		
		if(set_pos and node_to_move.has_method("set_position")):
			node_to_move.global_position = new_position_node.global_position
		if(set_rot and node_to_move.has_method("set_rotation_degrees")):
			node_to_move.global_rotation_degrees = new_position_node.global_rotation_degrees
	pass