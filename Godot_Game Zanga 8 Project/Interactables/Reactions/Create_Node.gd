extends "res://Interactables/Reactions/Abs_Reaction.gd"

export (PackedScene) var scene_to_create
export (NodePath) var parent_path

func _react():
	if(has_node(parent_path) and scene_to_create!=null):
		var parent = get_node(parent_path)
		var scene = scene_to_create.instance()
		parent.add_child(scene)
	pass