extends Area2D

signal completed_interaction
signal enabled
signal disabled
signal on_activity_set(new_activity)

export var debug=true
export var free_on_done = false
export var time_before_freeing = 5
export var calls_reactions = true
export var initial_activity = false

var reactions_completed = false
var permitted_to_call_reactions = true
var activity

var pointers = []

func _ready():
	if(not debug or not Global.debug):
		$"Debug Text".queue_free()
	
	var children = get_children()
	for child in children:
		if(child.is_in_group(Global.pointers_group)):
			pointers.append(child)
	
	set_activity(initial_activity)
	pass

func set_activity(activity):
	self.activity=activity
	emit_signal("on_activity_set",self.activity)
	if(activity):
		enable()
	else:
		disable()
	pass

func enable():
	for pointer in pointers:
		pointer.start()
	emit_signal("enabled")
	pass

func disable():
	for pointer in pointers:
		pointer.stop()
	emit_signal("disabled")
	pass

func interact():
	print("Base Interaction")
	_completed_interaction()
	pass

func _completed_interaction():
	emit_signal("completed_interaction")
	_try_call_reactions()
	pass

func _try_call_reactions():
	if(calls_reactions and permitted_to_call_reactions and has_node("Reactions")):
		var reactions_node = get_node("Reactions")
		if(not reactions_node.is_connected("all_reactions_completed",self,"_on_all_reactions_completed")):
			reactions_node.connect("all_reactions_completed",self,"_on_all_reactions_completed")
			reactions_node.react()
	else:
		_try_free()
	pass

func _on_all_reactions_completed():
	if(calls_reactions and permitted_to_call_reactions and has_node("Reactions")):
		var reactions_node = get_node("Reactions")
		if(reactions_node.is_connected("all_reactions_completed",self,"_on_all_reactions_completed")):
			reactions_node.disconnect("all_reactions_completed",self,"_on_all_reactions_completed")
	_try_free()
	pass

func _try_free():
	if(free_on_done):
		_free()
	pass

func _free():
	yield(get_tree().create_timer(time_before_freeing), "timeout")
	queue_free()
	pass