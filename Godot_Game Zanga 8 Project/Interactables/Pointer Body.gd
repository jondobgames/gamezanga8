extends Node2D

export var free_on_end = true
export var idle_anim="Idle"
export var start_anim = "Showing"
export var end_anim = "Vanishing"

var animation_player

func _ready():
	animation_player=$AnimationPlayer
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == end_anim and free_on_end):
		queue_free()
	elif(anim_name==start_anim):
		_try_play_anim(idle_anim)
	pass 

func stop():
	_try_play_anim(end_anim)
	pass

func start():
	_try_play_anim(start_anim)
	pass

func _try_play_anim(anim_name):
	if(animation_player.current_animation.get_basename()!=anim_name):
		animation_player.play(anim_name)
	pass