extends "res://Interactables/Abs_Interactable/Abs_Interactable.gd"

export var is_door_opened = false
export var open_anim="Open"
export var close_anim="Close"

var door_body
var animation_player

var start_mask
var start_layer

func _ready():
	animation_player = $AnimationPlayer
	door_body=$"Door Body"
	
	start_layer = door_body.get_collision_layer()
	start_mask = door_body.get_collision_mask()
	pass

func interact():
	_switch_door_state()
	pass

func set_door_state(to_state):
	if(is_door_opened != to_state):
		_switch_door_state()
	pass

func _switch_door_state():
	if(is_door_opened):
		_close_door()
	else:
		_open_door()
	pass

func _open_door():
	animation_player.play(open_anim)
	is_door_opened=true
	pass

func _close_door():
	animation_player.play(close_anim)
	is_door_opened=false
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	_completed_interaction()
	if(anim_name==open_anim):
		_disable_body()
	elif(anim_name==close_anim):
		_enable_body()
	pass

func _disable_body():
	door_body.set_collision_mask(0)
	door_body.set_collision_layer(0)
	pass

func _enable_body():
	door_body.set_collision_mask(start_mask)
	door_body.set_collision_layer(start_layer)
	pass