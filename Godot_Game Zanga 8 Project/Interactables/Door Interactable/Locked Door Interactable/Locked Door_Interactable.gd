extends "res://Interactables/Door Interactable/Door_Interactable.gd"

var selection_puzzle_interacter

func _ready():
	selection_puzzle_interacter=$"Selection Puzzle Interacter"
	selection_puzzle_interacter.connect("left_puzzle",self,"_on_left_selection_puzzle")
	selection_puzzle_interacter.connect("good_answer",self,"_on_good_answer")
	pass

func interact():
	if (not is_door_opened and not selection_puzzle_interacter.solved):
		selection_puzzle_interacter.open_puzzle()
	else:
		_switch_door_state()
	pass

func _on_good_answer():
	set_door_state(true)
	pass

func _on_left_selection_puzzle():
	_completed_interaction()
	pass