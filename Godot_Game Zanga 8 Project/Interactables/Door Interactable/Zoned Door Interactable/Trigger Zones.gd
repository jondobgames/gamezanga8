extends Area2D

export (NodePath) var interactable_node = get_parent()
signal completed_interaction

var activity

func get_interactable():
	return get_node(interactable_node)

func interact():
	get_interactable().interact()
	pass

func _set_activity(activity):
	self.activity=activity
	pass

func _on_activity_set(new_activity):
	activity=new_activity
	pass

func _on_completed_interaction():
	emit_signal("completed_interaction")
	pass 