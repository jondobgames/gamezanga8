extends "res://Interactables/Abs_Interactable/Abs_Interactable.gd"

export var story_key = "key"


func interact():
	if(Global.GUI.story_teller.is_connected("hid_teller",self,"_on_finished_telling") == false):
		Global.GUI.story_teller.connect("hid_teller",self,"_on_finished_telling")
	Global.GUI.tell_story(Global.get_story(story_key))
	pass

func _on_finished_telling():
	_completed_interaction()
	pass