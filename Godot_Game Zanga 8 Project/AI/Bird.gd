extends Area2D

export var active_anim_name = "Top_Down_Chase"

export var random_wait_time=true
export var min_rand=0.25
export var max_rand=1.25
export var stable_wait_time = 0

var animation_player 

func _ready():
	animation_player = get_parent().get_node("AnimationPlayer")
	pass

func _on_Bird_Area_body_entered(body):
	body.re_set_position()
	pass 

func _on_AnimationPlayer_animation_finished(anim_name):
	var wait_time = 0
	
	if(random_wait_time):
		randomize()
		wait_time = rand_range(min_rand,max_rand)
	else:
		wait_time=stable_wait_time
	
	yield(get_tree().create_timer(wait_time),"timeout")
	animation_player.play(active_anim_name)
	pass